package ee.ttu.tt.harjutused;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Test;

public class Tests {
	
	@Test
	public void testOnePlusOne() {
		assertEquals("Result is different then expected", 2, Exercises.onePlusOne());
	}
	
	@Test
	public void oneFactorial() {
		assertEquals("Factorial of 1 must be 1", 1, Exercises.factorial(1));
	}
	
	@Test
	public void twoFactorial() {
		assertEquals("Factorial of 2 must be 2", 2, Exercises.factorial(2));
	}
	
	@Test
	public void threeFactorial() {
		assertEquals("Factorial of 3 must be 6", 6, Exercises.factorial(3));
	}
	
	@Test
	public void oneEvenNum() {
		assertEquals("sum of 2 must be 2",2, Exercises.sum(new int[] {2}));
	}
	
	@Test
	public void multiEvenNum() {
		assertEquals("sum of 2,4,6 must be 12", 12, Exercises.sum(new int[] {2,4,6}));
	}
	
	@Test
	public void ignoreNegEvenNum() {
		assertEquals("sum of -20,4 must be 4", 4, Exercises.sum(new int[] {-20,4}));
	}
	
	@Test
	public void ignoreUnevensEvenNum() {
		assertEquals("sum of 1,3,4 must be 4", 4, Exercises.sum(new int[] {1,3,4}));
	}
	
	@Test
	public void ignoreUnevensAndNegEvenNum() {
		assertEquals("sum of -1,1,2,-3,4 must be 6", 6, Exercises.sum(new int[] {1,1,2,-3,4}));
	}
	
	@Test
	public void emptyArraySort() {
		assertArrayEquals("sorted {} is {}", new int[] {}, Exercises.sort(new int[] {}));
	}
	
	@Test
	public void oneArraySort() {
		assertArrayEquals("sorted {123} is {123}", new int[] {123}, Exercises.sort(new int[] {123}));
	}
	
	@Test
	public void threeUnsortedArraySort() {
		assertArrayEquals("unsorted {123,1,51} is {1,51,123}", new int[] {1,51,123}, Exercises.sort(new int[] {123,1,51}));
	}
	
	@Test
	public void threeSortedArraySort() {
		assertArrayEquals("sorted {1,51,123} is {1,51,123}", new int[] {1,51,123}, Exercises.sort(new int[] {1,51,123}));
	}
	
	@Test
	public void twoUnsortedArraySort() {
		assertArrayEquals("unsorted {123,51} is {51,123}", new int[] {51,123}, Exercises.sort(new int[] {123,51}));
	}
	
	@Test
	public void twoSortedArraySort() {
		assertArrayEquals("sorted {51,123} is {51,123}", new int[] {51,123}, Exercises.sort(new int[] {51,123}));
	}
	
	@Test
	public void longUnsortedArraySort() {
		assertArrayEquals("unsorted {1,3,4,5,6,2,8,7,9} is {1,2,3,4,5,6,7,8,9}", new int[] {1,2,3,4,5,6,7,8,9}, Exercises.sort(new int[] {1,3,4,5,6,2,8,7,9}));
	}
}
